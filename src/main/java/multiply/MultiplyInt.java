package multiply;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class MultiplyInt {
    private static final Logger logger = LogManager.getLogger(MultiplyImpl.class);

    public List<Number> multiplyInt(int min, int max, int inc) {
        LinkedList<Number> list = new LinkedList<>();
        if (min == 0 || max == 0 || inc == 0) throw new ArithmeticException();
        logger.info("start multiplication");
        for (int x = min; x <= max; x = x + inc) {
            for (int i = min; i <= max; i = i + inc) {
                list.add(x * i);
                logger.trace(x + " * " + i + " = " + x * i);
            }
        }
        logger.info("multiplication completed");
        return list;
    }

}
