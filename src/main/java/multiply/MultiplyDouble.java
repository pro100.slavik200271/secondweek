package multiply;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class MultiplyDouble {
    private static final Logger logger = LogManager.getLogger(MultiplyDouble.class);

    public List<Number> multiplyDouble(double min, double max, int inc) {
        LinkedList<Number> list = new LinkedList<>();
        if (min == 0 || max == 0 || inc == 0) throw new ArithmeticException();
        logger.info("start multiplication");
        for (double x = min; x <= max; x = x + inc) {
            for (double i = min; i <= max; i = i + inc) {
                list.add(x * i);
                logger.trace(x + " * " + i + " = " + x * i);
            }
        }
        logger.info("multiplication completed");
        return list;
    }
}
