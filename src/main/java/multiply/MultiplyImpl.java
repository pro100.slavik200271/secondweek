package multiply;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class MultiplyImpl implements Multiply {
    private static final Logger logger = LogManager.getLogger(MultiplyImpl.class);

    @Override
    public List<Number> multiply(String min, String max, int inc, String type) {
        switch (type) {
            case "double":
                logger.trace("multiply type - double");
                return new MultiplyDouble().multiplyDouble(Double.parseDouble(min), Double.parseDouble(max), inc);
            case "long":
                logger.trace("multiply type - long");
                return new MultiplyLong().multiplyLong(Long.parseLong(min), Long.parseLong(max), inc);
            case "short":
                logger.trace("multiply type - short");
                return new MultiplyShort().multiplyShort(Short.parseShort(min), Short.parseShort(max), inc);
            default: {
                logger.trace("multiply type - int");
                return new MultiplyInt().multiplyInt(Integer.parseInt(min), Integer.parseInt(max), inc);
            }
        }
    }


}
