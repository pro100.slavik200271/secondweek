package multiply;

import java.util.List;

public interface Multiply {
    List<Number> multiply(String min, String max, int inc, String type);
}
