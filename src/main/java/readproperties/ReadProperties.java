package readproperties;

import constants.Constants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class ReadProperties {
    private static final Logger logger = LogManager.getLogger(ReadProperties.class);
    public static final String PROPERTY_PATH = "src/main/resources/app.properties";
    //public static final String PROPERTY_PATH = new File(".").getCanonicalPath() + File.separator + "app.properties";


    private String min;
    private String max;
    private String increment;

    public ReadProperties() {
        readProperties();
    }

    private void readProperties() {
        try (FileInputStream fileInputStream = new FileInputStream(PROPERTY_PATH);) {
            logger.info("start reading properties");
            Properties myProps = new Properties();
            logger.trace("reading properties from file");
            myProps.load(new InputStreamReader(fileInputStream, StandardCharsets.UTF_8));
            min = myProps.getProperty(Constants.MIN);
            max = myProps.getProperty(Constants.MAX);
            increment = myProps.getProperty(Constants.INCREMENT);
            logger.debug(Constants.MIN + "-" + min);
            logger.debug(Constants.MAX + "-" + max);
            logger.debug(Constants.INCREMENT + "-" + increment);
            logger.info("loading properties completed");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public String getMin() {
        return min;
    }

    public String getMax() {
        return max;
    }

    public int getIncrement() {
        return Integer.parseInt(increment);
    }
}
