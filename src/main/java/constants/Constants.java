package constants;

public class Constants {
    private Constants() {
    }

    public static final String MIN = "min";
    public static final String MAX = "max";
    public static final String INCREMENT = "increment";
}
