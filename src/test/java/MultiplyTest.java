import multiply.Multiply;
import multiply.MultiplyImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class MultiplyTest {
    private Multiply multiply;

    @BeforeEach
    public void setUp() {
        multiply = new MultiplyImpl();
    }

    @Test
    void isRightSize() {
        Assertions.assertEquals(9, multiply.multiply("2", "6", 2, "int").size());
    }

    @Test
    void testMultiplyCorrect() {
        List<Number> list = multiply.multiply("9", "13", 4, "double");
        Assertions.assertTrue(checkMultiplyResult(list));
    }

    @Test
    void testShortMultiply() {
        List<Number> list = multiply.multiply("3", "12", 3, "short");
        try {
            list.get(0).shortValue();
            Assertions.assertTrue(true);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    void testLongMultiply() {
        List<Number> list = multiply.multiply("3", "12", 3, "long");
        try {
            list.get(0).longValue();
            Assertions.assertTrue(true);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    private boolean checkMultiplyResult(List<Number> list) {
        return list.get(0).intValue() == 81 && list.get(1).intValue() == 117 &&
                list.get(2).intValue() == 117 && list.get(3).intValue() == 169;
    }
}
