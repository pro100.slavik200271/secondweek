import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import readproperties.ReadProperties;

class ReadPropertiesTest {
    private ReadProperties readProperties;


    @BeforeEach
    public void setUp() {
        readProperties = new ReadProperties();
    }

    @Test
    void isMinCorrect() {
        Assertions.assertTrue(Double.parseDouble(readProperties.getMin()) != 0);
    }

    @Test
    void isPropertyFileCorrect(){
        Assertions.assertTrue(readProperties.PROPERTY_PATH.contains(".properties"));
    }

    @Test
    void isMaxCorrect() {
        Assertions.assertTrue(Double.parseDouble(readProperties.getMax()) != 0);
    }

    @Test
    void isIncrementCorrect() {
        Assertions.assertTrue(readProperties.getIncrement() != 0);
    }

}
